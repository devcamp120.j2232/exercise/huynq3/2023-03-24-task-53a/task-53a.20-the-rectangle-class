package com.devcamp.task53a20;

public class Rectangle {
    private float length = 1.0f;
    private float width = 1.0f;
    //Phương thức khởi tạo có tham số
    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }
     //Phương thức khởi tạo không có tham số
    public Rectangle() {
        super();
    }
    public float getLength() {
        return length;
    }
    public void setLength(float length) {
        this.length = length;
    }
    public float getWidth() {
        return width;
    }
    public void setWidth(float width) {
        this.width = width;
    }
    //Phương thức tính diện tích hcn
    public double getArea(){
        return length * width;
    }
    //Phương thức tính chu vi hcn
    public double getPerimeter(){
        return (length + width) * 2;
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return String.format("Rectangle [length= %s, width = %s]" , length, width);
    }
}
