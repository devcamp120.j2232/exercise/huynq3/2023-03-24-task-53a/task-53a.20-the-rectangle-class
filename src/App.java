import com.devcamp.task53a20.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        //diện tích 
        double dientich1 = rectangle1.getArea();
        double dientich2 = rectangle2.getArea();
        //chu vi
        double chuvi1 = rectangle1.getPerimeter();
        double chuvi2 = rectangle2.getPerimeter();
        System.out.println("Diện tích của rectangle 1:" + dientich1 + ", Chu vi: " + chuvi1);
        System.out.println("Diện tích của rectangle 2:" + dientich2 + ", Chu vi: " + chuvi2);
    }
}
